// @flow
import React, { Component } from 'react';
import { Checkbox, Button, Select, Progress, Alert } from 'antd';
import Ddragon from 'ddragon';
import fs from 'fs';
import styles from './Home.css';

const { dialog } = require('electron').remote;

const dd = new Ddragon();
const { Option } = Select;

const updatableItems = [
  'champions',
  'championsFull',
  'item',
  'summoner',
  'map',
  'runes',
  'language',
  'sticker',
  'profileicon'
];
const state = {};
updatableItems.forEach(item => (state[item] = true));

const downloadTask = (dataUrl, directory, onComplete) =>
  fetch(dataUrl)
    .then(result => result.json())
    .then(json => {
      const urlParts = dataUrl.split('/');
      fs.writeFile(
        `${directory}\\${urlParts[urlParts.length - 1]}`,
        JSON.stringify(json),
        () => {
          onComplete();
        }
      );
    });

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  state = {
    ...state,
    alerts: [],
    versions: [],
    selectedVersion: null,
    progress: 0,
    directory: ''
  };

  componentDidMount = () => {
    fetch(dd.versions(), {
      headers: {
        'Cache-Control': 'no-cache'
      }
    })
      .then(result => result.json())
      .then(versions =>
        this.setState({ versions, selectedVersion: versions[0] })
      )
      .catch(console.error);
    const storedDirectory = localStorage.getItem('directory');
    if (storedDirectory) this.setState({ directory: storedDirectory });
  };

  handleChange = e => {
    this.setState({
      [e.target.value]: e.target.checked
    });
  };

  selectDirectory = () => {
    dialog.showOpenDialog(
      {
        properties: ['openDirectory']
      },
      filePaths => {
        if (filePaths) {
          this.setState({ directory: filePaths[0] });
          localStorage.setItem('directory', filePaths[0]);
        }
      }
    );
  };

  onDownload = () => {
    this.setState({ progress: 0 }, () => {
      const { selectedVersion, directory } = this.state;
      if (selectedVersion) {
        dd.version = selectedVersion;
      }
      const downloadURLs = [];
      updatableItems.forEach(item => {
        if (this.state[item]) {
          downloadURLs.push(dd.data[item]());
        }
      });
      const downloadTasks = downloadURLs.map((url, index, array) =>
        downloadTask(url, directory, () =>
          this.setState({
            progress: this.state.progress + 100 / array.length
          })
        )
      );
      Promise.all(downloadTasks).catch(error => {
        console.log(error);
      });
    });
  };

  changeVersion = version => {
    this.setState({ selectedVersion: version });
  };

  render() {
    return [
      this.state.directory === '' && (
        <Alert banner message="You must select a directory before updating" />
      ),
      <div className={styles.container} data-tid="container">
        <h2>Data Dragon Downloader</h2>
        <span className={styles.directorySelect}>
          <Button onClick={this.selectDirectory} size="small">
            Select Directory
          </Button>
          <span className={styles.directory}>{this.state.directory}</span>
        </span>
        <p>Select which items you wish to update.</p>
        <div className={styles.checkBoxes}>
          {updatableItems.map(item => (
            <div className={styles.checkBoxContainer} key={item}>
              <Checkbox
                value={item}
                checked={this.state[item]}
                onChange={this.handleChange}
              >
                {item}
              </Checkbox>
            </div>
          ))}
        </div>
        <Select
          style={{ width: 120, marginRight: 5 }}
          value={this.state.selectedVersion}
          onChange={this.changeVersion}
        >
          {this.state.versions.map(version => (
            <Option value={version} key={version}>
              {version}
            </Option>
          ))}
        </Select>
        <Button
          type="primary"
          onClick={this.onDownload}
          disabled={this.state.directory === ''}
        >
          Update
        </Button>
        <Progress percent={+this.state.progress.toFixed(2)} size="small" />
      </div>
    ];
  }
}
